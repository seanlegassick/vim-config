if has('gui_running')
    set guioptions-=T
    set guioptions-=m
    set guioptions-=r
    set guioptions-=L
    if has("gui_macvim")
        if filereadable('/Users/seanl/.vim/opts/bigfont')
            set guifont=Menlo_Regular:h14
        elseif filereadable('/Users/seanl/.vim/opts/mediumfont')
            set guifont=Menlo_Regular:h12
        else
            set guifont=Menlo_Regular:h11
        endif 
        set fuoptions=maxvert,maxhorz
        if bufname('%') == ""
            set lines=77 columns=153
        else
            set lines=68 columns=100
        endif
        if filereadable('/Users/seanl/.vim/opts/fullscreen')
            au GUIEnter * set fullscreen
        endif
        if filereadable('/Users/seanl/.vim/opts/light')
            colorscheme macvim
            set background=light
        else
            colorscheme ir_black_sean
        endif
        map <silent><D-S-Right> :tabnext<CR>
        map <silent><D-S-Left> :tabprevious<CR>
    endif

    if has("gui_motif")
        set guifont=terminus-12
        colorscheme ir_black_sean
    endif

    if has("gui_gtk")
        set guioptions-=r
        set guioptions-=L
        if filereadable('/home/seanl/.bigfonts')
            set guifont=Terminus\ 10
        else
            set guifont=Terminus\ 8
        endif
        colorscheme ir_black_sean
    endif

    "TlistOpen
    "TlistClose

    "set title titlestring=%<%f\ %([%{Tlist_Get_Tagname_By_Line()}]%)
else
    if $TERM == 'xterm-256color'
        set t_Co=256
        "set t_AB=^[[48;5;%dm
        "set t_AF=^[[38;5;%dm
        colorscheme tir_black
    else
        colorscheme koehler_sean
    endif
endif

