"general settings
syntax on
set encoding=utf-8
"tab/indent settings
set expandtab
set shiftwidth=4
set smartindent
set softtabstop=4
set tabstop=8
set shiftround
set autoindent
"activate modelines
set modeline
"ignore case in search
set ignorecase
"ignore case only when pattern all lower-case
set smartcase
"don't wrap lines longer than window width
set nowrap
"fancier completion of menus
set wildmenu
"show extra stuff about cmd in status line
set showcmd
"show which mode (insert etc) in status line
set showmode
"show line,col in status line
set ruler
"automatically read external changes to file if unedited
set autoread
"set scenarios where backspace will continue working
set backspace=eol,start,indent
"show line number
set number
"change buffer without saving
set hidden
"highlight search
set hls
"regexps more like egrep (e.g. . not \. matches any char)
set magic
"how long to show matching parens for (tenths of sec)
set matchtime=2
"Switch to appropriate tab for already-open buffer
set switchbuf=usetab
"Incremental search
set incsearch
"Global subst by default
set gdefault

"External includes/calls
call pathogen#helptags()
runtime ftplugin/man.vim

"Tag list options
let g:Tlist_Ctags_Cmd="ctags"
let g:Tlist_File_Fold_Auto_Close=1
let g:Tlist_Inc_Winwidth=0
let g:Tlist_Compact_Format=1
let g:Tlist_Auto_Highlight_Tag=1
let g:Tlist_Close_On_Select=0
let g:Tlist_Exit_OnlyWindow=1
let g:Tlist_Auto_Open=0
let g:Tlist_Process_File_Always=1

"MiniBufExplorer options
"let g:miniBufExplMapWindowNavVim=1
"let g:miniBufExplMapWindowNavArrows=1
"let g:miniBufExplMapCTabSwitchBufs=1
"let g:miniBufExplModSelTarget=1 

"NERDtree options
let g:NERDTreeWinPos="right"

"Buftabs
set laststatus=2
let g:buftabs_in_statusline = 1
let g:buftabs_only_basename = 1

"simplenote
let g:SimpleNoteUserName = "sean@datamage.net"
let g:SimpleNotePassword = "ashlawn"

"python syntax checker
let g:pcs_max_lines_full_check = 2000

"nicer fuzzy-finder list colors
"highlight Pmenu guifg=#7f7f7f guibg=#111133 ctermfg=white ctermbg=blue
"highlight PmenuSel guifg=#ffffff guibg=#000088 ctermfg=white ctermbg=cyan

"Which commands wrap round line
"set whichwrap+=<,>,h,l
"set wrap

"Hide/ignore some files
let g:netrw_list_hide='^\.,.*\.class$,.*\.swp$,.*\.pyc$,.*\.swo$,\.DS_Store,\.AppleDouble$'
set wildignore+=*.o,*.obj,*.pyc,*.pyo,.git,.svn

"display trailing whitespace and tabs
highlight SpecialKey ctermfg=DarkGray
set list listchars=tab:\|_,trail:.

"enable filetype plugin and indent files
filetype plugin on
filetype indent on
"make sure to redo syntax highlighting on buffer enter
autocmd BufEnter * :syntax sync fromstart
"save on lost focus
"autocmd FocusLost * :wa

"custom keymap
"let mapleader=","

nnoremap <tab> %
vnoremap <tab> %
"nnoremap ; :

inoremap jj <ESC>

" stop auto-unindent of # lines
inoremap # X#

"move around windows
map <C-j>       <C-W>j
map <C-k>       <C-W>k
map <C-h>       <C-W>h
map <C-l>       <C-W>l
map <C-Down>    <C-W>j
map <C-Up>      <C-W>k
map <C-Left>    <C-W>h
map <C-Right>   <C-W>l

imap <C-Down>    <ESC><C-W>j
imap <C-Up>      <ESC><C-W>k
imap <C-Left>    <ESC><C-W>h
imap <C-Right>   <ESC><C-W>l

map <C-M-j>     <C-W>+
map <C-M-k>     <C-W>-
map <C-M-h>     <C-W><
map <C-M-l>     <C-W>>
map <C-M-Down>  <C-W>+
map <C-M-Up>    <C-W>-
map <C-M-Left>  <C-W><
map <C-M-Right> <C-W>>

nmap     <silent> <backspace>     :bnext<CR>
nnoremap <silent> <leader><space> :noh<CR>
map      <silent> <leader>a=      :call Align#AlignCtrl("mp1l:")<CR>:'<,'>Align =<CR>
map      <silent> <leader>a:      :call Align#AlignCtrl("mp0l:")<CR>:'<,'>Align :<CR>
nmap              <leader>cd      :cd `dirname %`<CR>
map               <leader>cp      "+y
nmap     <silent> <leader>db      :bdelete<CR>
nmap     <silent> <leader>dt      :tabclose<CR>
nmap     <silent> <leader>dw      :qa<CR>
nmap     <silent> <leader>ds      :%s/ \+$//<CR>
nmap     <silent> <leader>e       :NERDTreeFind<CR>
nmap     <silent> <leader>f-      :set guifont=Menlo_Regular:h11<CR>
nmap     <silent> <leader>f+      :set guifont=Menlo_Regular:h14<CR>
nmap     <silent> <leader>ff      :FufFileWithFullCwd<CR>
nmap     <silent> <leader>fd      :FufFileWithCurrentBufferDir<CR>
nmap              <leader>fx      :!chmod +x %<CR>
nmap     <silent> <leader>g       :FufTag<CR>
nmap     <silent> <leader>G       :FufTagWithCursorWord<CR>
nmap     <silent> <leader>l       :TlistOpen<CR>
nmap     <silent> <leader>nb      :enew<CR>
nmap     <silent> <leader>nt      :tabnew<CR>
nmap     <silent> <leader>nw      :silent !gvim<CR>
nmap     <silent> <leader>ni      :setl noai nocin nosi inde=
nmap              <leader>p       "+gp
nmap     <silent> <leader>q       :FufBufferTag<CR>
nmap     <silent> <leader>r       :MRU<CR>
nmap     <silent> <leader>s       :wa<CR>
nmap     <silent> <leader>v       :e ~/.vim/plugin/a_conf/vimrc.vim<CR>
map               <leader>y       "+y

nnoremap          <leader>xt      :call RunDjangoTests('', '')<cr>:redraw<cr>:call JumpToError()<cr>
nnoremap          <leader>xf      :call RunDjangoTestsForFile('--failfast')<cr>:redraw<cr>:call JumpToError()<cr>

nnoremap <silent> ,,              ,
nmap     <silent> ,x              :bnext<CR>
nmap     <silent> ,z              :bprev<CR>

map      <silent> <F8>            :TlistOpen<CR>
map      <silent> <F7>            :bprevious<CR>
imap     <silent> <F7>            <ESC>:bprevious<CR>
map      <silent> <F9>            :bnext<CR>
imap     <silent> <F9>            <ESC>:bnext<CR>

map      <silent> <C-PageUp>       :bprevious<CR>
imap     <silent> <C-PageUp>       <ESC>:bprevious<CR>
map      <silent> <C-PageDown>     :bnext<CR>
imap     <silent> <C-PageDown>     <ESC>:bnext<CR>


