#!/bin/sh
ssh $1 <<EOF
cd .vim
git pull
git submodule update --init
grep -v minibufexpl .git/config > /tmp/gc
mv /tmp/gc .git/config
rm -rf bundle/minibufexpl
[ $UID = '0' ] && aptitude install -y ruby ruby-dev
cd bundle/command-t/ruby/command-t
ruby extconf.rb
make
EOF


